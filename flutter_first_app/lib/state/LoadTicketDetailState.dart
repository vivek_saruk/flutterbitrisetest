import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_first_app/pojo/LoadTicketDetailsEntity.dart';
import 'package:flutter_first_app/utils/AppString.dart';
import 'package:flutter_first_app/utils/DataUtil.dart';
import 'package:flutter_first_app/utils/colors.dart';
import 'package:flutter_first_app/widgets/LoadTicketWidget.dart';
import 'package:intl/intl.dart';

class LoadTicketDetailState extends State<LoadTicketWidget> {
  String _farmLocation;
  String _selectedCustomer;
  String _selectedGrower;
  String _selectedField;
  String _selectedCrop;
  String _selectedVariety;
  String _selectedLot;

  var _formKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime.now();
  DateTime plannedArrivalDate = DateTime.now();
  DateTime actualArrivalDate = DateTime.now();
  DateTime deliveryDate = DateTime.now();
  DateTime loadingCompletedDate = DateTime.now();

  DateFormat dateFormat = DateFormat.yMMMd();
  TimeOfDay selectedTimeOfDay = TimeOfDay.now();
  TimeOfDay plannedArrivalTimeOfDay = TimeOfDay.now();
  TimeOfDay actualArrivalTimeOfDay = TimeOfDay.now();
  TimeOfDay deliveryTimeOfDay = TimeOfDay.now();
  TimeOfDay loadingCompletedTimeOfDay = TimeOfDay.now();

  TextEditingController plannedArrivalController = new TextEditingController();
  TextEditingController actualArrivalController = new TextEditingController();
  TextEditingController deliveryDateController = new TextEditingController();
  TextEditingController loadingCompletedDateController =
      new TextEditingController();

  List<String> farmList;
  List<String> customerList;
  List<String> growerList;
  List<String> fieldList;
  List<String> cropList;
  List<String> varietyList;
  List<String> lotList;

  LoadTicketDetailEntity _loadTicketDetailEntity;
  DataUtil _dataUtil;

  Future<Null> datePicker(String forWidget) async {
    DateTime pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime(2100),
      builder: (BuildContext buildContext, Widget widget) {
        return Theme(
          data: ThemeData.fallback(),
          child: widget,
        );
      },
    );

    if (pickedDate != null) {
      setState(() {
        selectedDate = pickedDate;

        switch (forWidget) {
          case AppString.PLANNED_ARRIVAL_ON_SITE:
            plannedArrivalDate = selectedDate;
            break;

          case AppString.ACTUAL_ARRIVAL:
            actualArrivalDate = selectedDate;
            break;

          case AppString.DELIVERY_DATE:
            deliveryDate = selectedDate;
            break;

          case AppString.LOADING_COMPLETED:
            loadingCompletedDate = selectedDate;
            break;
        }

        timePicker(forWidget);
      });
    }
  }

  Future<Null> timePicker(String forWidget) async {
    TimeOfDay pickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext buildContext, Widget child) {
        return Directionality(
          textDirection: prefix0.TextDirection.rtl,
          child: child,
        );
      },
    );

    setState(() {
      if (pickedTime != null) {
        selectedTimeOfDay = pickedTime;
        String time = selectedTimeOfDay.format(context);
        String date = dateFormat.format(selectedDate) + " " + time;

        switch (forWidget) {
          case AppString.PLANNED_ARRIVAL_ON_SITE:
            plannedArrivalTimeOfDay = selectedTimeOfDay;
            plannedArrivalController.text = date;
            break;

          case AppString.ACTUAL_ARRIVAL:
            actualArrivalTimeOfDay = selectedTimeOfDay;
            actualArrivalController.text = date;
            break;

          case AppString.DELIVERY_DATE:
            deliveryTimeOfDay = selectedTimeOfDay;
            deliveryDateController.text = date;
            break;

          case AppString.LOADING_COMPLETED:
            loadingCompletedTimeOfDay = selectedTimeOfDay;
            loadingCompletedDateController.text = date;
            break;
        }

        _formKey.currentState.validate();
      }
    });
  }

  TextStyle getHeaderTextStyle() =>
      TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 20);

  TextStyle getGreyTextStyle() => TextStyle(
      fontFamily: 'roboto',
      fontStyle: FontStyle.normal,
      fontSize: 16,
      fontWeight: FontWeight.normal,
      color: AppColors.greyColor);

  LoadTicketDetailState() {
    _loadTicketDetailEntity = LoadTicketDetailEntity();
    _dataUtil = DataUtil();

    farmList = _dataUtil.getFarmList();
    _farmLocation = farmList.first;

    customerList = _dataUtil.getCustomerList();
    _selectedCustomer = customerList.first;

    growerList = _dataUtil.getGrowerList();
    _selectedGrower = growerList.first;

    fieldList = _dataUtil.getFieldList();
    _selectedField = fieldList.first;

    cropList = _dataUtil.getCropList();
    _selectedCrop = cropList.first;

    varietyList = _dataUtil.getVarietyList();
    _selectedVariety = varietyList.first;

    lotList = _dataUtil.getLotList();
    _selectedLot = lotList.first;
  }

  Future<Void> alertDialog() {
    return showDialog(
        context: context,
        builder: (BuildContext buildContext) {
          return AlertDialog(
            title: Text(AppString.LOAD_TICKET_DIALOG_TITLE),
            content: Text(AppString.LOAD_TICKET_DAILOG_CONTENT),
            actions: <Widget>[
              FlatButton(
                child: Text(AppString.YES),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text(AppString.NO),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  Future<bool> _onBackPressed() async {
    // Your back press code here...
    alertDialog();
    return true;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    plannedArrivalController.dispose();
    actualArrivalController.dispose();
    deliveryDateController.dispose();
    loadingCompletedDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Color.fromARGB(255, 230, 230, 230),
            appBar: AppBar(
              title: Text(AppString.CREATE_LOAD_TICKET),
              leading: IconButton(
                icon: new Icon(Icons.arrow_back),
                onPressed: () {
                  alertDialog();
                },
              ),
            ),
            body: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              padding: EdgeInsets.only(bottom: 10.0),
              child: Form(
                key: _formKey,
                child: Container(
                  margin: EdgeInsets.only(left: 5.0, right: 5.0, top: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 20.0),
                        child: Text(
                          AppString.PICKUP,
                          style: getHeaderTextStyle(),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        height: 300.0,
                        margin: EdgeInsets.only(top: 10.0),
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.LOCATIONS,
                                    style: getGreyTextStyle(),
                                  ),
                                  DropdownButtonFormField(
                                    key: Key(AppString.LOCATIONS),
                                    items: farmList
                                        .map<DropdownMenuItem<String>>(
                                            (String location) {
                                      return DropdownMenuItem(
                                        value: location,
                                        child: Text(location),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _farmLocation = value;
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    onSaved: (item) {
                                      setState(() {
                                        _loadTicketDetailEntity.location = item;
                                      });
                                    },
                                    validator: (String selectedItem) {
                                      return selectedItem == null ||
                                              selectedItem.isEmpty
                                          ? AppString.LOCATION_ERROR
                                          : null;
                                    },
                                    value: _farmLocation,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.PLANNED_ARRIVAL_ON_SITE,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    keyboardType: TextInputType.datetime,
                                    autofocus: false,
                                    focusNode:
                                        FocusNode(canRequestFocus: false),
                                    key: Key(AppString.PLANNED_ARRIVAL_ON_SITE),
                                    onTap: () {
                                      datePicker(
                                          AppString.PLANNED_ARRIVAL_ON_SITE);
                                    },
                                    validator: (text) {
                                      return text.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    controller: plannedArrivalController,
                                    onSaved: (text) {
                                      setState(() {
                                        _loadTicketDetailEntity.plannedArrival =
                                            text;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.ACTUAL_ARRIVAL,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    keyboardType: TextInputType.datetime,
                                    autofocus: false,
                                    focusNode:
                                        FocusNode(canRequestFocus: false),
                                    key: Key(AppString.ACTUAL_ARRIVAL),
                                    onTap: () {
                                      datePicker(AppString.ACTUAL_ARRIVAL);
                                    },
                                    onSaved: (text) {
                                      if (text.isNotEmpty) {
                                        _loadTicketDetailEntity.actualArrival =
                                            text;
                                      }
                                    },
                                    controller: actualArrivalController,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20.0, top: 10.0),
                        child: Text(
                          AppString.DELIVERY,
                          style: getHeaderTextStyle(),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        margin: EdgeInsets.only(top: 10.0),
                        color: Colors.white,
                        height: 320.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              AppString.SELECT_CUSTOMER,
                              style: getGreyTextStyle(),
                            ),
                            DropdownButtonFormField(
                              key: Key(AppString.SELECT_CUSTOMER),
                              items: customerList.map<DropdownMenuItem<String>>(
                                  (String customer) {
                                return DropdownMenuItem(
                                  value: customer,
                                  child: Text(customer),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedCustomer = value;
                                  _formKey.currentState.validate();
                                });
                              },
                              validator: (String selectedCustomer) {
                                return selectedCustomer == null ||
                                        selectedCustomer.isEmpty
                                    ? AppString.CUSTOMER_ERROR
                                    : null;
                              },
                              onSaved: (selectedValue) {
                                setState(() {
                                  _loadTicketDetailEntity.selectedCustomer =
                                      selectedValue;
                                });
                              },
                              value: _selectedCustomer,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.DELIVERY_DESTINATION,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 150,
                                    key: Key(AppString.DELIVERY_DESTINATION),
                                    onSaved: (text) {
                                      setState(() {
                                        if (text.isNotEmpty) {
                                          _loadTicketDetailEntity.deliveryDate =
                                              text;
                                        }
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.DELIVERY_DATE,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                      keyboardType: TextInputType.datetime,
                                      autofocus: false,
                                      focusNode:
                                          FocusNode(canRequestFocus: false),
                                      key: Key(AppString.DELIVERY_DATE),
                                      onTap: () {
                                        datePicker(AppString.DELIVERY_DATE);
                                      },
                                      validator: (date) {
                                        return date.isEmpty
                                            ? AppString.MANDATORY_ERROR
                                            : null;
                                      },
                                      onSaved: (date) {
                                        setState(() {
                                          _loadTicketDetailEntity.deliveryDate =
                                              date;
                                        });
                                      },
                                      controller: deliveryDateController)
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20.0, top: 10.0),
                        child: Text(
                          AppString.CROP_PASSPORT,
                          style: getHeaderTextStyle(),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        color: Colors.white,
                        margin: EdgeInsets.only(top: 10.0),
                        height: 680.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              AppString.SELECT_GROWER,
                              style: getGreyTextStyle(),
                            ),
                            DropdownButtonFormField(
                              key: Key(AppString.SELECT_GROWER),
                              items: growerList.map<DropdownMenuItem<String>>(
                                  (String customer) {
                                return DropdownMenuItem(
                                  value: customer,
                                  child: Text(customer),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedGrower = value;
                                  _formKey.currentState.validate();
                                });
                              },
                              validator: (String selectedValue) {
                                return selectedValue == null ||
                                        selectedValue.isEmpty
                                    ? AppString.MANDATORY_ERROR
                                    : null;
                              },
                              onSaved: (String selectedValue) {
                                setState(() {
                                  _loadTicketDetailEntity.selectedGrower =
                                      selectedValue;
                                });
                              },
                              value: _selectedGrower,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.AP_NUMBER,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    key: Key(AppString.AP_NUMBER),
                                    onSaved: (text) {
                                      setState(() {
                                        if (text.isNotEmpty) {
                                          _loadTicketDetailEntity.apNumber =
                                              text;
                                        }
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.FIELD,
                                    style: getGreyTextStyle(),
                                  ),
                                  DropdownButtonFormField(
                                    key: Key(AppString.FIELD),
                                    items: fieldList
                                        .map<DropdownMenuItem<String>>(
                                            (String field) {
                                      return DropdownMenuItem(
                                        value: field,
                                        child: Text(field),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedField = value;
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    validator: (String selectedValue) {
                                      return selectedValue == null ||
                                              selectedValue.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    onSaved: (String selectedValue) {
                                      setState(() {
                                        _loadTicketDetailEntity.field =
                                            selectedValue;
                                      });
                                    },
                                    value: _selectedField,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.CROP,
                                    style: getGreyTextStyle(),
                                  ),
                                  DropdownButtonFormField(
                                    key: Key(AppString.CROP),
                                    items: cropList
                                        .map<DropdownMenuItem<String>>(
                                            (String crop) {
                                      return DropdownMenuItem(
                                        value: crop,
                                        child: Text(crop),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedCrop = value;
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    validator: (String selectedValue) {
                                      return selectedValue == null ||
                                              selectedValue.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    onSaved: (String selectedValue) {
                                      setState(() {
                                        _loadTicketDetailEntity.crop =
                                            selectedValue;
                                      });
                                    },
                                    value: _selectedCrop,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.VARIETY,
                                    style: getGreyTextStyle(),
                                  ),
                                  DropdownButtonFormField(
                                    key: Key(AppString.VARIETY),
                                    items: varietyList
                                        .map<DropdownMenuItem<String>>(
                                            (String variety) {
                                      return DropdownMenuItem(
                                        value: variety,
                                        child: Text(variety),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedVariety = value;
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    validator: (String selectedValue) {
                                      return selectedValue == null ||
                                              selectedValue.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    onSaved: (String selectedValue) {
                                      setState(() {
                                        _loadTicketDetailEntity.variety =
                                            selectedValue;
                                      });
                                    },
                                    value: _selectedVariety,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.LOT,
                                    style: getGreyTextStyle(),
                                  ),
                                  DropdownButtonFormField(
                                    key: Key(AppString.LOT),
                                    items: lotList
                                        .map<DropdownMenuItem<String>>(
                                            (String lot) {
                                      return DropdownMenuItem(
                                        value: lot,
                                        child: Text(lot),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedLot = value;
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    validator: (String selectedValue) {
                                      return selectedValue == null ||
                                              selectedValue.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    onSaved: (String selectedValue) {
                                      setState(() {
                                        _loadTicketDetailEntity.lot =
                                            selectedValue;
                                      });
                                    },
                                    value: _selectedLot,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.COUNTRY_OF_ORIGIN,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    key: Key(AppString.COUNTRY_OF_ORIGIN),
                                    onSaved: (String text) {
                                      if (text.isNotEmpty) {
                                        setState(() {
                                          _loadTicketDetailEntity
                                              .countryOrigin = text;
                                        });
                                      }
                                    },
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20.0, top: 10.0),
                        child: Text(
                          AppString.LOAD_DETAILS,
                          style: getHeaderTextStyle(),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        color: Colors.white,
                        margin: EdgeInsets.only(top: 10.0),
                        height: 680.0,
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.LOAD_NUMBER,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 150,
                                    key: Key(AppString.LOAD_NUMBER),
                                    onChanged: (text) {
                                      setState(() {
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    validator: (String text) {
                                      return text == null || text.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    onSaved: (String text) {
                                      setState(() {
                                        _loadTicketDetailEntity.loadNumber =
                                            text;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.TONNAGE,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 2,
                                    keyboardType: TextInputType.number,
                                    key: Key(AppString.TONNAGE),
                                    validator: (String text) {
                                      if (text == null || text.isEmpty) {
                                        return AppString.MANDATORY_ERROR;
                                      } else {
                                        int tonnage = int.parse(text);
                                        return tonnage > 50
                                            ? AppString.TONNAGE_ERROR
                                            : null;
                                      }
                                    },
                                    onChanged: (text) {
                                      setState(() {
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    onSaved: (String text) {
                                      int tonnage = int.parse(text);
                                      setState(() {
                                        _loadTicketDetailEntity.tonnage =
                                            tonnage;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.HAULIER,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 150,
                                    key: Key(AppString.HAULIER),
                                    onChanged: (text) {
                                      setState(() {
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    onSaved: (text) {
                                      setState(() {
                                        _loadTicketDetailEntity.haulier = text;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.VEHICLE_REGISTRATION,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 150,
                                    key: Key(AppString.VEHICLE_REGISTRATION),
                                    validator: (text) {
                                      return text == null || text.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                    onChanged: (text) {
                                      setState(() {
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    onSaved: (text) {
                                      setState(() {
                                        _loadTicketDetailEntity
                                            .vehicleRegistration = text;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.TRAILER_NUMBER,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 150,
                                    key: Key(AppString.TRAILER_NUMBER),
                                    onChanged: (text) {
                                      setState(() {
                                        _formKey.currentState.validate();
                                      });
                                    },
                                    onSaved: (text) {
                                      setState(() {
                                        _loadTicketDetailEntity.trailerNumber =
                                            text;
                                      });
                                    },
                                    validator: (text) {
                                      return text == null || text.isEmpty
                                          ? AppString.MANDATORY_ERROR
                                          : null;
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.SEAL_NUMBER,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    maxLength: 150,
                                    key: Key(AppString.SEAL_NUMBER),
                                    onSaved: (text) {
                                      if (text != null && text.isNotEmpty) {
                                        _loadTicketDetailEntity.sealNumber =
                                            text;
                                      }
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    AppString.LOADING_COMPLETED,
                                    style: getGreyTextStyle(),
                                  ),
                                  TextFormField(
                                    keyboardType: TextInputType.datetime,
                                    autofocus: false,
                                    focusNode:
                                        FocusNode(canRequestFocus: false),
                                    key: Key(AppString.LOADING_COMPLETED),
                                    controller: loadingCompletedDateController,
                                    onTap: () {
                                      datePicker(AppString.LOADING_COMPLETED);
                                    },
                                    onSaved: (date) {
                                      setState(() {
                                        _loadTicketDetailEntity
                                            .loadingCompleted = date;
                                      });
                                    },
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20.0, top: 10.0),
                        child: Text(
                          AppString.LOAD_TICKET_COMMENTS,
                          style: getHeaderTextStyle(),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        color: Colors.white,
                        margin: EdgeInsets.only(top: 10.0),
                        height: 200.0,
                        child: Scrollbar(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            reverse: true,
                            child: SizedBox(
                              height: 190,
                              child: TextFormField(
                                maxLength: 500,
                                maxLines: 8,
                                onSaved: (comment) {
                                  setState(() {
                                    _loadTicketDetailEntity.loadTicketComment =
                                        comment;
                                  });
                                },
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Add your text here"),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20.0, top: 10.0),
                        child: Text(
                          AppString.INTERNAL_NOTES,
                          style: getHeaderTextStyle(),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        color: Colors.white,
                        margin: EdgeInsets.only(top: 10.0),
                        height: 200.0,
                        child: Scrollbar(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            reverse: true,
                            child: SizedBox(
                              height: 190,
                              child: TextFormField(
                                maxLength: 500,
                                maxLines: 8,
                                onSaved: (text) {
                                  setState(() {
                                    _loadTicketDetailEntity.internalNotes =
                                        text;
                                  });
                                },
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Add your text here"),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 50,
                        margin: EdgeInsets.only(top: 50),
                        child: RaisedButton(
                          color: Colors.blueAccent,
                          textColor: Colors.white,
                          child: Text(
                            "Confirm & Send",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          onPressed: () {
                            _formKey.currentState.validate();
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )));
  }
}
