class DataUtil {
  List<String> getFarmList() {
    List<String> list = new List();
    list.add("");
    list.add("Nasik");
    list.add("Pune");
    list.add("Mumbai");
    list.add("Aurangabad");
    list.add("Nagpur");
    return list;
  }

  List<String> getCustomerList() {
    List<String> list = new List();
    list.add("");
    list.add("Lisa beattie");
    list.add("Rohit Sinha");
    list.add("Nisahnt Thite");
    list.add("Sumedh Dharmadhikari");
    return list;
  }

  List<String> getGrowerList() {
    List<String> list = new List();
    list.add("");
    list.add("Lisa beattie");
    list.add("Rohit Sinha");
    list.add("Nisahnt Thite");
    list.add("Sumedh Dharmadhikari");
    return list;
  }

  List<String> getFieldList() {
    List<String> list = new List();
    list.add("");
    list.add("FIELD-1");
    list.add("FIELD-2");
    list.add("FIELD-3");
    list.add("FIELD-4");
    list.add("FIELD-5");
    list.add("FIELD-6");
    list.add("FIELD-7");
    return list;
  }

  List<String> getCropList() {
    List<String> list = new List();
    list.add("");
    list.add("Potato");
    list.add("Orange");
    list.add("Banana");
    list.add("Rice");
    list.add("Wheat");
    return list;
  }

  List<String> getVarietyList() {
    List<String> list = new List();
    list.add("");
    list.add("Variety-1");
    list.add("Variety-2");
    list.add("Variety-3");
    list.add("Variety-4");
    list.add("Variety-5");
    return list;
  }

  List<String> getLotList() {
    List<String> list = new List();
    list.add("");
    list.add("Lot-12345");
    list.add("Lot-12346");
    list.add("Lot-12347");
    list.add("Lot-12348");
    list.add("Lot-12349");
    return list;
  }
}
