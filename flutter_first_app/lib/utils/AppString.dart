class AppString {
  static const CREATE_LOAD_TICKET = "Create load ticket";
  static const PICKUP = "Pickup";
  static const LOCATIONS = "Locations";
  static const PLANNED_ARRIVAL_ON_SITE = "Planned arrival on site";
  static const ACTUAL_ARRIVAL = "Actual arrival on site";

  static const DELIVERY = "Delivery";
  static const SELECT_CUSTOMER = "Select customer";
  static const DELIVERY_DESTINATION = "Delivery Destination (Optional)";
  static const DELIVERY_DATE = "Delivery date";

  static const CROP_PASSPORT = "Crop passport";
  static const SELECT_GROWER = "Select grower";
  static const AP_NUMBER = "AP number";
  static const FIELD = "Field";
  static const CROP = "Crop";
  static const VARIETY = "Variety";
  static const LOT = "Lot";
  static const COUNTRY_OF_ORIGIN = "Country of origin";

  static const LOAD_DETAILS = "Load details";
  static const LOAD_NUMBER = "Load number";
  static const TONNAGE = "Tonnage";
  static const HAULIER = "Haulier";
  static const VEHICLE_REGISTRATION = "Vehicle registration";
  static const TRAILER_NUMBER = "Trailer number";
  static const SEAL_NUMBER = "Seal number (Optional)";
  static const LOADING_COMPLETED = "Loading completed";

  static const LOAD_TICKET_COMMENTS = "Load ticket comment (Optional)";
  static const INTERNAL_NOTES = "Internal notes (Optional)";

  static const ENTER = "Enter here";

  static const LOAD_TICKET_DIALOG_TITLE = "Load Ticket";
  static const LOAD_TICKET_DAILOG_CONTENT = "Do you really want to exit";

  static const YES = "Yes";
  static const NO = "No";

  static const MANDATORY_ERROR = "This field is required";
  static const LOCATION_ERROR = "Select a location";
  static const CUSTOMER_ERROR = "Select a customer";
  static const TONNAGE_ERROR = "Value would not be greater than 50";
}
