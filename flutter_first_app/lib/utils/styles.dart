import 'dart:ui';

import 'package:flutter_first_app/utils/colors.dart';

class Styles {
  static TextStyle getGreyTextStyle = TextStyle(
      fontFamily: 'roboto',
      fontStyle: FontStyle.normal,
      fontSize: 16,
      fontWeight: FontWeight.normal,
      color: AppColors.greyColor);
}
