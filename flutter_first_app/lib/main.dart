import 'package:flutter/material.dart';
import 'package:flutter_first_app/widgets/LoadTicketWidget.dart';

void main() => runApp(FlutterFormValidationApp());

class FlutterFormValidationApp extends StatelessWidget {
  final String appName = "Flutter Form Validation";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      home: Scaffold(
        body: Center(
          child: LoadTicketWidget(),
        ),
      ),
    );
  }
}
