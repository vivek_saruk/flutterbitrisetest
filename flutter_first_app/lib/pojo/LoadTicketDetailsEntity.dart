import 'dart:core';

class LoadTicketDetailEntity {
  String location;
  String plannedArrival;
  String actualArrival;
  String selectedCustomer;
  String deliveryDestination;
  String deliveryDate;
  String selectedGrower;
  String apNumber;
  String field;
  String crop;
  String variety;
  String lot;
  String countryOrigin;
  String loadNumber;
  int tonnage;
  String haulier;
  String vehicleRegistration;
  String trailerNumber;
  String sealNumber;
  String loadingCompleted;
  String loadTicketComment;
  String internalNotes;
}
